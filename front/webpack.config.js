const webpack = require("webpack");
const path = require("path");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const OptimizeCssAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");

const config = {
	entry: {
		main: "./app/index.js",
		vendor: "moment"
	},
	output: {
		filename: "[name].[chunkhash].js",
		path: path.resolve(__dirname, "dist")
	},
	module: {
        rules: [		
			{test: /\.css$/, use: ExtractTextPlugin.extract({use: "css-loader"})},
			{test: /\.(jpg|png|gif)$/, use: "file-loader"},
			{test: /\.(woff|woff2|eot|ttf|svg)$/, use: {loader: "url-loader", options: {limit: 100000}}}
		]
    },
	plugins: [		
		//new webpack.optimize.UglifyJsPlugin(),
		new ExtractTextPlugin("styles.css"),
		new OptimizeCssAssetsPlugin({
			assetNameRegExp: /\.css$/,
			cssProcessor: require("cssnano"),
			cssProcessorOptions: {discardComments: {removeAll: true}},
			canPrint: true
		}),
		new HtmlWebpackPlugin({
			//title: "",
			template: "./app/index.html",
			minify: {
				collapseWhitespace: true,
				removeComments: true,
				removeRedundantAttributes: true,
				removeScriptTypeAttributes: true,
				removeStyleLinkTypeAttributes: true
			}
		}),
		new webpack.optimize.CommonsChunkPlugin({
			name: "vendor",
			minChunks: function (module) {			
				return module.context && module.context.indexOf("node_modules") !== -1;
			}
		}),	
		new webpack.optimize.CommonsChunkPlugin({ 
			name: "manifest"
		}),
		new CopyWebpackPlugin([
			{ from: "./app/lib", to: "lib" },
			{ from: "./app/js", to: "js" },
			{ from: "./app/views", to: "views" }
        ], {
            ignore: [],
            copyUnmodified: true
        })
	]	
};

module.exports = config;

define(["./module"], function (controllers) {
    "use strict";   

    controllers.controller("MainCtrl", ["$scope", "$http", "$route", "$timeout", "$location", "$window", "md5", "DataSet", "jQuery", function ($scope, $http, $route, $timeout, $location, $window, md5, DataSet, $) {
		$scope.сurrencies = [];

		$scope.apiEndpoint = "http://127.0.0.1:9000/api";

        $scope.user = {
			id: 1,
			email: "user@site.net",
            passwordHash: "",
            password: "user@site.net",
            name: "user@site.net"
        };      

		var passwordHash = md5.createHash($scope.user.password);
		$scope.user.passwordHash = passwordHash;	

        $scope.login = function () {			
            $("#loginDialog").modal("hide");

            var passwordHash = md5.createHash($scope.user.password);       
            $http
                .post($scope.apiEndpoint + "/auth/login", {
                    email: $scope.user.email,                   
                    passwordHash: passwordHash
                })
                .then(function (data, status, headers, config) {
                    DataSet.instance().setData("token", data.data.token);
					$scope.loadData();
                })
                .catch(function (data, status, headers, config) {
                    delete DataSet.instance().getData("token");
                });		
        };

		$scope.loadData = function() {
			$http({url: $scope.apiEndpoint + "/currencies", method: "GET"})
				.then(function (data, status, headers, config) {
					$scope.сurrencies = data.data;		
								
				})
				.catch(function (data, status, headers, config) {              
					console.error(data);
				});
		};
	

		$scope.createCurrency = function() {
			$("#сurrencyEditor #id").val("");
			$("#сurrencyEditor #сurrency-name").val("");
			$("#сurrencyEditor #сurrency-volume").val("");
			$("#сurrencyEditor #сurrency-amount").val("");

			$("#сurrencyEditor").modal();
		}

        $scope.editCurrency = function(id) {
            for ( var i = 0; i < $scope.сurrencies.length; i++ ) {
                var currentItem = $scope.сurrencies[i];              
                if ( currentItem.id === id ) {
					$("#сurrencyEditor #id").val(currentItem.id);
					$("#сurrencyEditor #сurrency-name").val(currentItem.name);
					$("#сurrencyEditor #сurrency-volume").val(currentItem.volume);
					$("#сurrencyEditor #сurrency-amount").val(currentItem.amount);
					
                    break;
                }
            }

            $("#сurrencyEditor").modal();
        };

        $scope.saveCurrency = function() {           
            $("#сurrencyEditor").modal("hide");			

			var сurrency = {
				id: $("#сurrencyEditor #id").val(),
				name: $("#сurrencyEditor #сurrency-name").val(),
				volume: $("#сurrencyEditor #сurrency-volume").val(),
				amount: $("#сurrencyEditor #сurrency-amount").val()
			}
		
			if ( сurrency.id.length == 0 ) {
				$http
					.post($scope.apiEndpoint + "/currencies",
						сurrency
					)
					.then(function (data, status, headers, config) {					
						$scope.сurrencies.push(сurrency);
					})
					.catch(function (data, status, headers, config) {
						console.error(data);
					});
			}
			else {
				$http
					.put($scope.apiEndpoint + "/currencies",
						сurrency
					)
					.then(function (data, status, headers, config) {
						for ( var i = 0; i < $scope.сurrencies.length; i++ ) {
							var currentItem = $scope.сurrencies[i];              
							if ( currentItem.id === id ) {
								$scope.сurrencies[i] = сurrency;
								break;
							}
						}
					})
					.catch(function (data, status, headers, config) {
						console.error(data);
					});
			}			
        }

        $scope.deleteCurrency = function(id) {           
			$http
				.delete($scope.apiEndpoint + "/currencies/" + id,
					{}
				)
				.then(function (data, status, headers, config) {					
					for ( var i = 0; i < $scope.сurrencies.length; i++ ) {
						var currentItem = $scope.сurrencies[i];              
						if ( currentItem.id === id ) {
							delete $scope.сurrencies.splice( i, 1 );
							break;
						}
					}
				})
				.catch(function (data, status, headers, config) {
					console.error(data);
				});
        };	

		$scope.loadData();
    }]);	
});

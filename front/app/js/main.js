require.config({
    paths: {
        "angular": "../lib/angular/angular",
        "angular-route": "../lib/angular-route/angular-route",
        "angular-md5": "../lib/angular-md5/angular-md5",      
        "domReady": "../lib/requirejs-domready/domReady"
    },
    shim: {
        "angular": {
            exports: "angular"
        },
        "angular-route": {
            deps: ["angular"]
        },
        "angular-md5": {
            deps: ["angular"]
        }
    },
    deps: [       
        "./bootstrap"
    ]
});
define(["./app"], function (app) {
    "use strict";
    return app.config(["$routeProvider", function ($routeProvider) {
        $routeProvider.when("/", {
            templateUrl: "views/main.html",
            controller: "MainCtrl"
        });

        $routeProvider.when("/settings", {
            templateUrl: "views/settings.html",
            controller: "SettingsCtrl"
        });

        $routeProvider.otherwise({
            redirectTo: "/"
        });
    }]);
});

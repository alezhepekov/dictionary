define([
    "angular",
    "angular-route",
    "angular-md5",
    "./controllers/index",
    "./directives/index",
    "./filters/index",
    "./services/index"
], function (angular) {
    "use strict";

    var app = angular.module("app", [
        "app.controllers",
        "app.directives",
        "app.filters",
        "app.services",
        "ngRoute",
        "ngMd5"
    ]);

    app.constant("jQuery", window.jQuery);

    app.factory("DataSet", function() {        
        function DataSetInstance() {
            this.data = {};          
        }

        DataSetInstance.prototype.setData = function(name, data) {
            this.data[name] = data;
        }

        DataSetInstance.prototype.getData = function(name) {
            return this.data[name];
        }

        DataSetInstance.prototype._instance = null;      

        return {
            instance: function() {              
                if (!DataSetInstance.prototype._instance) {
                    DataSetInstance.prototype._instance = new DataSetInstance();                   
                }
                return DataSetInstance.prototype._instance;
            }          
        };
    });

    app.factory("authInterceptor", function ($rootScope, $q, $window, DataSet) {    
        return {
            request: function (config) {           
                config.headers = config.headers || {};
                var token = DataSet.instance().getData("token");                      
                if (token) {
                    config.headers.Authorization = "Bearer " + token;             
                }
                console.log(config);
                return config;
            },
            response: function (response) {
                console.log(response);  
                return response || $q.when(response);
            },
            responseError: function(rejection) {
                console.log(rejection);  
                if (rejection.status === 401 || rejection.status === 403) {                
                    $("#loginDialog").modal();
                }
                return $q.reject(rejection);
            }
        };
    })

    app.config(["$locationProvider", "$httpProvider", function($locationProvider, $httpProvider) {
        $locationProvider.hashPrefix("");
        $httpProvider.interceptors.push("authInterceptor");       
    }]);

    return app;
});

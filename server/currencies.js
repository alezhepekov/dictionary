const express = require("express");
const router = express.Router();
const bodyParser = require("body-parser");
const dbdatamanager = require("./dbdatamanager");
const authcontrol = require("./authcontrol");
const uuidV4 = require("uuid/v4");
const jwt = require("jsonwebtoken");

router.use(function (req, res, next) {
    var headerAuthorization = req.header("Authorization");
    if ( !headerAuthorization ) {
        return res.status(401).end(JSON.stringify({}));
    }

    var items = headerAuthorization.split(" ");
    var authType = items[0];
    if ( authType !== "Bearer" ) {          
        return res.status(400).end(JSON.stringify({error:{message:"Недопустимый метод авторизации"}}));           
    }
    var token = items[1];     
    jwt.verify(token, "secret", function(error, decodedToken) {
        if ( error ) {
            return res.status(400).end(JSON.stringify({error:{message:"Некорректный токен"}}));
        }         

        if ( decodedToken.exp < Math.floor(Date.now() / 1000) ) {            
            return res.status(403).end(JSON.stringify({error:{message:"Время действия токена истекло"}}));
        }

        var accessToken = token;   
        authcontrol.checkAuthorization(accessToken, (result, error)=>{
            if (error) {
                return res.status(500).end(JSON.stringify({}));
            }

            if (result.auth && result.auth == false) {
                return res.status(401).end(JSON.stringify({}));
            }
          
            next();
        });	
    });
});

router.use(bodyParser.json());

router.get("/", function(req, res) {
    var ftx = function(req, res) {
        var query = {};
        var dbDataManager = new dbdatamanager.DBDataManager();
        dbDataManager.selectСurrencies(query, (result, error) => {
            if ( error ) {
                console.log(error);
                if ( error.status && error.message ) {
                    return res.status(error.status).end(error.message);
                }
                else {
                    return res.status(500).end(JSON.stringify({}));
                }
            }

            var outString = "[";
            for ( var i = 0; i < result.length; i++ ) {
                var currentItem = result[i];		
                outString += JSON.stringify(currentItem);
                if ( i < result.length - 1 ) {
                    outString += ",";
                }			
            }
            outString += "]";
            
            res.status(200).end(outString);
        });
    };

    ftx(req, res);
});

router.get("/:id", function(req, res) {
    var ftx = function(req, res) {
        if (!req.params.id) {
            res.status(400).end(JSON.stringify({}));
        }

        var query = {"id": req.params.id};
        var dbDataManager = new dbdatamanager.DBDataManager();
        dbDataManager.selectСurrency(query, (result, error) => {
            if ( error ) {
                console.log(error);
                if ( error.status && error.message ) {
                    return res.status(error.status).end(error.message);
                }
                else {
                    return res.status(500).end(JSON.stringify({}));
                }
            }
        
            res.status(200).end(JSON.stringify(result));	
        });
    };

    ftx(req, res);
});

router.post("/", function(req, res) {
    var ftx = function(req, res) {
        if (!req.body) {
            res.status(400).end(JSON.stringify({}));
        }
        
        var data = req.body;

        if ( !data.id ) {
            data.id = uuidV4();
        }

        var dbDataManager = new dbdatamanager.DBDataManager();
        dbDataManager.insertСurrency(data, (result, error) => {
            if ( error ) {
                console.log(error);
                if ( error.status && error.message ) {
                    return res.status(error.status).end(error.message);
                }
                else {
                    return res.status(500).end(JSON.stringify({}));
                }
            }

            res.status(201).end(JSON.stringify({id: data.id}));
        });
    };

    ftx(req, res);
});

router.put("/", function(req, res) {
    var ftx = function(req, res) {
        if (!req.body) {
            res.status(400).end(JSON.stringify({}));
        }	
        
        var data = req.body;
        var query = { "id" : data.id };
        var dbDataManager = new dbdatamanager.DBDataManager();
        dbDataManager.updateСurrency(query, data, (result, error) => {
            if ( error ) {
                console.log(error);
                if ( error.status && error.message ) {
                    return res.status(error.status).end(error.message);
                }
                else {
                    return res.status(500).end(JSON.stringify({}));
                }
            }
            
            res.status(200).end(JSON.stringify({}));
        });
    };

    ftx(req, res);
});

router.delete("/:id", function(req, res) {
    var ftx = function(req, res) {
        if (!req.params.id) {
            res.status(400).end(JSON.stringify({}));
        }
        
        var query = {"id": req.params.id};
        var dbDataManager = new dbdatamanager.DBDataManager();
        dbDataManager.deleteСurrency(query, (result, error) => {
            if ( error ) {
                console.log(error);
                if ( error.status && error.message ) {
                    return res.status(error.status).end(error.message);
                }
                else {
                    return res.status(500).end(JSON.stringify({}));
                }
            }

            res.status(200).end(JSON.stringify({}));
        });
    };

    ftx(req, res);
});

module.exports = router;

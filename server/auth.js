const express = require("express");
const router = express.Router();
const bodyParser = require("body-parser");
const dbdatamanager = require("./dbdatamanager");
const crypto = require("crypto");
const hash = crypto.createHash("sha256");
const uuidV4 = require("uuid/v4");
const jwt = require("jsonwebtoken");

router.use(function (req, res, next) {
	res.setHeader("Content-Type", "application/json");
	next();
});

router.use(bodyParser.json());

router.post("/login", function(req, res) {
    if (!req.body) {
		res.status(400).end(JSON.stringify({}));
	}

    var user = req.body;
    var query = {"email": user.email};
	var dbDataManager = new dbdatamanager.DBDataManager();
	dbDataManager.selectUser(query, (result, error) => {
        if ( error ) {
            console.log(error);
            if ( error.status && error.status == 404 ) {
                return res.status(403).end(JSON.stringify({error: "Пользователь не существует"}));
            }
            else {
                return res.status(500).end(JSON.stringify({}));
            }
        }

        var data = result;
        if ( user.passwordHash.toUpperCase() === data.passwordHash.toUpperCase() ) {
            jwt.sign({
                    exp: Math.floor(Date.now() / 1000) + (60 * 60), // Время действия 1 час
                    data: user.email
                },
                "secret",
                {},
                (error, token) => {
                    if ( error ) {
						return res.status(500).end(JSON.stringify({}));
                    }

                    console.log(token);
					var connection = {
                        id: uuidV4(),
                        userID: user.email,
                        accessToken: token,
                        connectionTime: Math.floor(Date.now() / 1000)
                    };

                    var data = connection;
                    dbDataManager.insertConnection(data, (result, error) => {
                        if ( error ) {
                            console.log(error);
                            if ( error.status && error.message ) {
                                return res.status(error.status).end(error.message);
                            }
                            else {
                                return res.status(500).end(JSON.stringify({}));
                            }
                        }

                        return res.status(200).end(JSON.stringify({token: token}));
                    }); 
                }
            );
        }
        else {
            res.status(403).end(JSON.stringify({}));   
        }
    });  
});

router.post("/logout", function(req, res) {
    if (!req.body) {
		res.status(400).end(JSON.stringify({}));
	}

    var data = req.body;
   
    var query = {"accessToken": data.token};
	var dbDataManager = new dbdatamanager.DBDataManager();
	dbDataManager.deleteConnection(query, (result, error) => {
        if ( error ) {
            console.log(error);
            if ( error.status && error.message ) {
                return res.status(error.status).end(error.message);
            }
            else {
                return res.status(500).end(JSON.stringify({}));
            }
        }

        return res.status(200).end(JSON.stringify({}));
    });  
});

router.post("/register", function(req, res) {
    if (!req.body) {
		res.status(400).end(JSON.stringify({}));
	}

    var user = req.body;
    var query = {"email": user.email};
	var dbDataManager = new dbdatamanager.DBDataManager();
	dbDataManager.selectUser(query, (result, error) => {
        if ( error ) {
            if ( error.status && error.status == 404 ) {
                var data = user;

                if ( !data.id ) {
                    data.id = uuidV4();
                }

                dbDataManager.insertUser(data, (result, error) => {
                    if ( error ) {
                        console.log(error);
                        if ( error.status && error.message ) {
                            return res.status(error.status).end(error.message);
                        }
                        else {
                            return res.status(500).end(JSON.stringify({}));
                        }
                    }              
                
                    res.status(201).end(JSON.stringify({id: data.id}));	
                });
            }
            else {
                if ( error.status && error.message ) {
                    return res.status(error.status).end(error.message);
                }
                else {
                    return res.status(500).end(JSON.stringify({}));
                }
            }            
        }

        res.status(409).end(JSON.stringify({})); // CONFLICT Already exists       	
	});
});

module.exports = router;

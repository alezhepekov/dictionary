const MongoClient = require("mongodb").MongoClient;
const assert = require("assert");
const ObjectId = require("mongodb").ObjectID;
const config = require('./config');

var url = "mongodb://" + config.DBHost + ":" + config.DBPort + "/" + config.DBName;

function DBDataManager() {
}

/* Connections */

var selectConnections = function(query, db, cb) {
	var result = [];
	var cursor = db.collection("connections").find(query);
	cursor.each(function(error, object) {		
		if (error) {
			return cb && cb(null, error);
		}

		if (object != null) {
			result.push(object);
		} else {		
			cb && cb(result);
		}		
	});	
};

DBDataManager.prototype.selectConnections = function(query, cb) {
	MongoClient.connect(url, function(error, db) {	
		if (error) {
			return cb && cb(null, error);
		}

		selectConnections(query, db, function(result, error) {
			db.close();
			if (error) {
				return cb && cb(null, error);
			}
			cb && cb(result);	
		});
	});
}

var selectConnection = function(query, db, cb) {
	var cursor = db.collection("connections").find(query);
    cursor.count(function(error, count) {
		if (error) {
			return cb && cb(null, error);
		}	

		if (count == 0) {	
			return cb && cb(null, {status: 404, message: "Not found"});	
		}

		cursor.nextObject(function(error, object) {
			if (error) {
				return cb && cb(null, error);
			}

			cb && cb(object);
		});
    });	
};

DBDataManager.prototype.selectConnection = function(query, cb) {	
	MongoClient.connect(url, function(error, db) {	
		if (error) {
			return cb && cb(null, error);
		}

		selectConnection(query, db, function(result, error) {
			db.close();
			if (error) {
				return cb && cb(null, error);
			}
			cb && cb(result);	
		});
	});
}

var insertConnection = function(data, db, cb) {
	var cursor = db.collection("connections").find({"id" : data.id});
	cursor.count(function(error, count) {
		if (error) {
			return cb && cb(null, error);
		}

		if (count != 0) {
			return cb && cb(null, {status: 409, message: "Already exists"});
		}

		db.collection("connections").insertOne(
			data,			
			function(error, result) {
				if (error) {
					return cb && cb(null, error);
				}
				cb && cb();
		});
    });	
};

DBDataManager.prototype.insertConnection = function(data, cb) {
	MongoClient.connect(url, function(error, db) {	
		if (error) {
			return cb && cb(null, error);
		}

		insertConnection(data, db, function(result, error) {
			db.close();
			if (error) {
				return cb && cb(null, error);
			}
			cb && cb();	
		});
	});
}

var updateConnection = function(query, data, db, cb) {
	var cursor = db.collection("connections").find(query);
	cursor.count(function(error, count) {
		if (error) {
			return cb && cb(null, error);
		}

		if (count == 0) {
			return cb && cb(null, {status: 404, message: "Not found"});
		}

		db.collection("connections").updateOne(
			query,
			{
				$set: {
					"id": data.id,
					"userID": data.userID,
					"accessToken": data.accessToken,
					"connectionTime": data.connectionTime
				},
				$currentDate: { "lastModified": true }
			}, function(error, results) {			
				if (error) {
					return cb && cb(null, error);
				}
				cb && cb();	
		});
    });
};

DBDataManager.prototype.updateConnection = function(query, data, cb) {
	MongoClient.connect(url, function(error, db) {		
		if (error) {
			return cb && cb(null, error);
		}

		updateConnection(query, data, db, function(result, error) {
			db.close();
			if (error) {
				return cb && cb(null, error);
			}
			cb && cb();			
		});
	});
}

var deleteConnection = function(query, db, cb) {
	var cursor = db.collection("connections").find(query);
	cursor.count(function(error, count) {
		if (error) {
			return cb && cb(null, error);
		}

		if (count == 0) {
			return cb && cb(null, {status: 404, message: "Not found"});
		}

		db.collection("connections").deleteOne(
			query,
			function(error, results) {
				if (error) {
					return cb && cb(null, error);
				}
				cb && cb();
			}
		);
    });
};

DBDataManager.prototype.deleteConnection = function(query, cb) {
	MongoClient.connect(url, function(error, db) {		
		if (error) {
			return cb && cb(null, error);
		}

		deleteConnection(query, db, function(result, error) {
			db.close();
			if (error) {
				return cb && cb(null, error);
			}
			cb && cb();
		});
	});
}

/* Users */

var selectUsers = function(query, db, cb) {
	var result = [];
	var cursor = db.collection("users").find(query);
	cursor.each(function(error, object) {		
		if (error) {
			return cb && cb(null, error);
		}

		if (object != null) {
			result.push(object);
		} else {		
			cb && cb(result);
		}		
	});	
};

DBDataManager.prototype.selectUsers = function(query, cb) {
	MongoClient.connect(url, function(error, db) {	
		if (error) {
			return cb && cb(null, error);
		}

		selectUsers(query, db, function(result, error) {
			db.close();
			if (error) {
				return cb && cb(null, error);
			}
			cb && cb(result);	
		});
	});
}

var selectUser = function(query, db, cb) {
	var cursor = db.collection("users").find(query);
    cursor.count(function(error, count) {
		if (error) {
			return cb && cb(null, error);
		}	

		if (count == 0) {	
			return cb && cb(null, {status: 404, message: "Not found"});	
		}

		cursor.nextObject(function(error, object) {
			if (error) {
				return cb && cb(null, error);
			}

			cb && cb(object);
		});
    });	
};

DBDataManager.prototype.selectUser = function(query, cb) {	
	MongoClient.connect(url, function(error, db) {	
		if (error) {
			return cb && cb(null, error);
		}

		selectUser(query, db, function(result, error) {
			db.close();
			if (error) {
				return cb && cb(null, error);
			}
			cb && cb(result);	
		});
	});
}

var insertUser = function(data, db, cb) {
	var cursor = db.collection("users").find({"id" : data.id});
	cursor.count(function(error, count) {
		if (error) {
			return cb && cb(null, error);
		}

		if (count != 0) {
			return cb && cb(null, {status: 409, message: "Already exists"});
		}

		db.collection("users").insertOne(
			data,			
			function(error, result) {
				if (error) {
					return cb && cb(null, error);
				}
				cb && cb();
		});
    });	
};

DBDataManager.prototype.insertUser = function(data, cb) {
	MongoClient.connect(url, function(error, db) {	
		if (error) {
			return cb && cb(null, error);
		}

		insertUser(data, db, function(result, error) {
			db.close();
			if (error) {
				return cb && cb(null, error);
			}
			cb && cb();	
		});
	});
}

var updateUser = function(query, data, db, cb) {
	var cursor = db.collection("users").find(query);
	cursor.count(function(error, count) {
		if (error) {
			return cb && cb(null, error);
		}

		if (count == 0) {
			return cb && cb(null, {status: 404, message: "Not found"});
		}

		db.collection("users").updateOne(
			query,
			{
				$set: {
					"id": data.id,
					"email": data.email,
					"passwordHash": data.passwordHash
				},
				$currentDate: { "lastModified": true }
			}, function(error, results) {			
				if (error) {
					return cb && cb(null, error);
				}
				cb && cb();	
		});
    });
};

DBDataManager.prototype.updateUser = function(query, data, cb) {
	MongoClient.connect(url, function(error, db) {		
		if (error) {
			return cb && cb(null, error);
		}

		updateUser(query, data, db, function(result, error) {
			db.close();
			if (error) {
				return cb && cb(null, error);
			}
			cb && cb();			
		});
	});
}

var deleteUser = function(query, db, cb) {
	var cursor = db.collection("users").find(query);
	cursor.count(function(error, count) {
		if (error) {
			return cb && cb(null, error);
		}

		if (count == 0) {
			return cb && cb(null, {status: 404, message: "Not found"});
		}

		db.collection("users").deleteOne(
			query,
			function(error, results) {
				if (error) {
					return cb && cb(null, error);
				}
				cb && cb();
			}
		);
    });
};

DBDataManager.prototype.deleteUser = function(query, cb) {
	MongoClient.connect(url, function(error, db) {		
		if (error) {
			return cb && cb(null, error);
		}

		deleteUser(query, db, function(result, error) {
			db.close();
			if (error) {
				return cb && cb(null, error);
			}
			cb && cb();
		});
	});
}

/* Сurrencies */

var selectСurrencies = function(query, db, cb) {
	var result = [];
	var cursor = db.collection("currencies").find(query);
	cursor.each(function(error, object) {		
		if (error) {
			return cb && cb(null, error);
		}

		if (object != null) {
			result.push(object);
		} else {		
			cb && cb(result);
		}		
	});	
};

DBDataManager.prototype.selectСurrencies = function(query, cb) {
	MongoClient.connect(url, function(error, db) {	
		if (error) {
			return cb && cb(null, error);
		}

		selectСurrencies(query, db, function(result, error) {
			db.close();
			if (error) {
				return cb && cb(null, error);
			}
			cb && cb(result);	
		});
	});
}

var selectCurrency = function(query, db, cb) {
	var cursor = db.collection("currencies").find(query);
    cursor.count(function(error, count) {
		if (error) {
			return cb && cb(null, error);
		}	

		if (count == 0) {	
			return cb && cb(null, {status: 404, message: "Not found"});	
		}

		cursor.nextObject(function(error, object) {
			if (error) {
				return cb && cb(null, error);
			}

			cb && cb(object);
		});
    });	
};

DBDataManager.prototype.selectCurrency = function(query, cb) {	
	MongoClient.connect(url, function(error, db) {	
		if (error) {
			return cb && cb(null, error);
		}

		selectCurrency(query, db, function(result, error) {
			db.close();
			if (error) {
				return cb && cb(null, error);
			}
			cb && cb(result);	
		});
	});
}

var insertCurrency = function(data, db, cb) {
	var cursor = db.collection("currencies").find({"id" : data.id});
	cursor.count(function(error, count) {
		if (error) {
			return cb && cb(null, error);
		}

		if (count != 0) {
			return cb && cb(null, {status: 409, message: "Already exists"});
		}

		db.collection("currencies").insertOne(
			data,			
			function(error, result) {
				if (error) {
					return cb && cb(null, error);
				}
				cb && cb();
		});
    });	
};

DBDataManager.prototype.insertCurrency = function(data, cb) {
	MongoClient.connect(url, function(error, db) {	
		if (error) {
			return cb && cb(null, error);
		}

		insertCurrency(data, db, function(result, error) {
			db.close();
			if (error) {
				return cb && cb(null, error);
			}
			cb && cb();	
		});
	});
}

var updateCurrency = function(query, data, db, cb) {
	var cursor = db.collection("currencies").find(query);
	cursor.count(function(error, count) {
		if (error) {
			return cb && cb(null, error);
		}

		if (count == 0) {
			return cb && cb(null, {status: 404, message: "Not found"});
		}

		db.collection("currencies").updateOne(
			query,
			{
				$set: {
					"id": data.id,
					"name": data.name,
					"volume": data.volume,
					"amount": data.amount				
				},
				$currentDate: { "lastModified": true }
			}, function(error, results) {			
				if (error) {
					return cb && cb(null, error);
				}
				cb && cb();	
		});
    });
};

DBDataManager.prototype.updateCurrency = function(query, data, cb) {
	MongoClient.connect(url, function(error, db) {		
		if (error) {
			return cb && cb(null, error);
		}

		updateCurrency(query, data, db, function(result, error) {
			db.close();
			if (error) {
				return cb && cb(null, error);
			}
			cb && cb();			
		});
	});
}

var deleteCurrency = function(query, db, cb) {
	var cursor = db.collection("currencies").find(query);
	cursor.count(function(error, count) {
		if (error) {
			return cb && cb(null, error);
		}

		if (count == 0) {
			return cb && cb(null, {status: 404, message: "Not found"});
		}

		db.collection("currencies").deleteOne(
			query,
			function(error, results) {
				if (error) {
					return cb && cb(null, error);
				}
				cb && cb();
			}
		);
    });
};

DBDataManager.prototype.deleteCurrency = function(query, cb) {
	MongoClient.connect(url, function(error, db) {		
		if (error) {
			return cb && cb(null, error);
		}

		deleteCurrency(query, db, function(result, error) {
			db.close();
			if (error) {
				return cb && cb(null, error);
			}
			cb && cb();
		});
	});
}

module.exports.DBDataManager = DBDataManager;

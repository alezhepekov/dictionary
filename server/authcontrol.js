var dbdatamanager = require("./dbdatamanager");

function checkAuthorization(accessToken, cb) {
    var query = {"accessToken": accessToken};
	var dbDataManager = new dbdatamanager.DBDataManager();
	dbDataManager.selectConnection(query, (result, error) => {
        if ( error ) {
            return cb && cb(null, error);
        }

        if (!result) {
            return cb && cb({auth: false});
        }
       
        cb && cb({auth: true});
    });   
}

module.exports.checkAuthorization = checkAuthorization;
